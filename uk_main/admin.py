from django.contrib import admin
from .models import *

# Register your models here.
from django.contrib.auth.admin import UserAdmin

admin.site.site_title = "Kalashnikov Diplom"
admin.site.site_header = "Kalashnikov Diplom"


# from .forms import CustomUserCreationForm, CustomUserChangeForm

# class CustomUserAdmin(UserAdmin):
#     add_form = CustomUserCreationForm
#     form = CustomUserChangeForm
#     model = CustomUser
#     list_display = ('email', 'is_staff', 'is_active',)
#     list_filter = ('email', 'is_staff', 'is_active',)
#     fieldsets = (
#         (None, {'fields': ('email', 'password')}),
#         ('Permissions', {'fields': ('is_staff', 'is_active')}),
#     )
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
#         ),
#     )
#     search_fields = ('email',)
#     ordering = ('email',)

admin.site.register(User)
admin.site.register(News)
admin.site.register(Notification)
admin.site.register(House)
admin.site.register(WaterMeter)
admin.site.register(UserRequests)
admin.site.register(NPA)
admin.site.register(PerfomedWork)
admin.site.register(Address)
admin.site.register(NewsComments)
admin.site.register(Chat)
admin.site.register(Message)
admin.site.register(HousesManage)
admin.site.register(ProfileImage)