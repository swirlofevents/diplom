from .models import (
    NewsComments,
    Address,
    House,
    News,
    Notification,
    NotificationRead,
    NPA,
    PerfomedWork,
    UserRequests,
    WaterMeter,
    Chat,
    Message,
    HousesManage,
    ProfileImage,
)

# Create your models here.
