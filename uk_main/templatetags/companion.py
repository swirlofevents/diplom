from ..models import Message, Chat
from django import template

register = template.Library()


@register.simple_tag
def get_companion(user, chat):
    for u in chat.members.all():
        if u != user:
            return u
    return None


@register.simple_tag
def get_sobes(user, chat_id):
    chat = Chat.objects.filter(id=chat_id).first()
    for member in chat.members.all():
        if member != user:
            return member
    # for message in messages:
    #     for member in message.chat.members.all():
    #         if member != user:
    #             return member


@register.simple_tag
def get_not(user):
    count_unreaded = 0
    chats = Chat.objects.filter(members__in=[user.id])
    for chat in chats:
        # получаем сообщения к чату
        messeges = Message.objects.filter(chat=chat).order_by("pub_date")
        # получаем последнее сообщение
        last_messages = messeges.last()
        if last_messages != None:
            if last_messages.author != user:
                if last_messages.is_readed == False:
                    count_unreaded += 1
    return count_unreaded