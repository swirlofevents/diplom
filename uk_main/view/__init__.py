from .main import MainView
from .npa import NPAView
from .perfomed_work import PerfomedWorkRetrive, PerfomedWorkRetriveByQuarterYear, PerfomedWorksListView
from .comments import CommentDeleteView, CreateCommentView
from .chats import CreateMessageView, ChatListView, ChatCreateView
from .requests import RequestStar, RequestDeleteView, RequestAddView, UserRequestsView, DispetherRequestsView
from .water_meters import WaterMeterAddView, WaterMeterView, WaterMeterDeleteView
from .news import NewsListView, NewsRetriveView
from .settings import SettingsView
from .license_requisites import LicenseView, RequisitesView
from .homes_manage import HomeRetriveView, HomeListView
