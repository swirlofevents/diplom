from django.shortcuts import render
from django.views.generic.base import View


class LicenseView(View):
    def get(self, request):
        return render(request, "informations/license.html")