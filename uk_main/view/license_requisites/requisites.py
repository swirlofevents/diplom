from django.shortcuts import render
from django.views.generic.base import View


class RequisitesView(View):
    def get(self, request):
        return render(request, "informations/requisites.html")