from django.shortcuts import redirect
from django.views.generic.base import View
from ...models import News
from ...forms import CreateCommentForm
from datetime import datetime


class CreateCommentView(View):
    """создание комментариев"""

    def post(self, request, news_id):
        form = CreateCommentForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.news = News.objects.get(id=news_id)
            form.user_id = request.user.id
            form.date = datetime.now()
            form.save()

        return redirect("/news/" + str(news_id))