from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.shortcuts import get_object_or_404
from ...models import NewsComments


class CommentDeleteView(View):
    """Удаление обращения жильца"""

    def post(self, request, comment_id):
        comment = get_object_or_404(NewsComments, pk=comment_id)
        news_pk = comment.news.id
        if comment:
            if comment.user.id == request.user.id:
                comment.delete()

        return redirect("/news/" + str(news_pk))