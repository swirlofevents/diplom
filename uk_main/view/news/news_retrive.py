from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.core.exceptions import ObjectDoesNotExist
from ...models import News


class NewsRetriveView(View):
    """Новость полная"""

    def get(self, request, news_id):
        try:
            news = News.objects.get(pk=news_id)
            comments_news = news.comments.all()
            comments = []
            for comment in comments_news:
                if comment.parent == None:
                    json_comment = {
                        "comment": comment,
                        "child_comments": comment.child_comments.all(),
                    }
                    comments.append(json_comment)
        except ObjectDoesNotExist:
            return redirect(
                "news",
            )
        return render(request, "news/news_read_more.html", {"news": news, "comments": comments})
