from django.shortcuts import render
from django.views.generic.base import View
from ...models import News, Notification
from datetime import date


class NewsListView(View):
    """Список новостей и уведомлений"""

    def get(self, request):
        news = News.objects.all().order_by("-publicate_date")
        now = date.today()
        notifications = Notification.objects.filter(date_end__gte=now)
        return render(
            request,
            "news/news_list.html",
            {
                "news_list": news,
                "notifications_list": notifications,
            },
        )