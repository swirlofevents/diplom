from django.shortcuts import render
from django.views.generic.base import View


class MainView(View):
    """
    Основная страница
    О компаниия
    """

    def get(self, request):
        return render(request, "about/about.html")
