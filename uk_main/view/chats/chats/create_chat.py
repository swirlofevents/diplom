from django.shortcuts import redirect, render
from django.views.generic.base import View
from ....models import Chat, Message, User


class ChatCreateView(View):
    def get(self, request, user_id):
        creater = request.user
        second_member = User.objects.get(id=user_id)
        old_chat = Chat.objects.filter(members__in=[creater]).filter(members__in=[second_member]).first()
        if old_chat == None:
            creat_chat = Chat(type="D")
            creat_chat.save()
            creat_chat.members.add(creater)
            creat_chat.members.add(second_member)
            creat_chat.save()
            return redirect("/ms/?im=" + str(creat_chat.id))
        else:
            return redirect("/ms/?im=" + str(old_chat.id))

    def post(self, request, user_id):
        creater = request.user
        second_member = User.objects.get(id=user_id)
        old_chat = Chat.objects.filter(members__in=[creater]).filter(members__in=[second_member]).first()
        if old_chat == None:
            creat_chat = Chat(type="D")
            creat_chat.save()
            creat_chat.members.add(creater)
            creat_chat.members.add(second_member)
            creat_chat.save()
            return redirect("/ms/?im=" + str(creat_chat.id))
        else:
            return redirect("/ms/?im=" + str(old_chat.id))
