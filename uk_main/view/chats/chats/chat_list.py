from django.shortcuts import render
from django.views.generic.base import View
from ....models import Chat, Message, User


class ChatListView(View):
    def get(self, request):
        try:
            chat_id = request.GET["im"]
        except:
            chat_id = ""
        all_users = User.objects.all()

        if chat_id:
            # получаем юзер айди
            print(chat_id)
            user_id = request.user.id
            # получаем чаты
            chats = Chat.objects.filter(members__in=[user_id]).order_by("-updated")
            # получаем конкретный чат
            chat = Chat.objects.get(id=chat_id)
            # получаем сообщения к чату
            messeges = Message.objects.filter(chat=chat).order_by("pub_date")
            # получаем последнее сообщение
            last_messages = messeges.last()
            # проверяем на прочитанность последнее сообщение
            if last_messages != None:
                if last_messages.author != request.user:
                    if last_messages.is_readed == False:
                        last_messages.is_readed = True
                        last_messages.save()
            return render(
                request,
                "chats/chats.html",
                {"chats": chats, "messeges": messeges, "chat_id": int(chat_id), "all_users": all_users},
            )
        else:
            user_id = request.user.id
            chats = Chat.objects.filter(members__in=[user_id]).order_by("-updated")
            return render(request, "chats/chats.html", {"chats": chats, "all_users": all_users})
