from django.shortcuts import render, redirect
from django.views.generic.base import View
from ....models import Message, Chat
from django.utils import timezone


class CreateMessageView(View):
    def post(self, request, chat_id):
        message = Message(chat_id=chat_id, author=request.user, message=request.POST["message"])
        message.save()
        chat = Chat.objects.get(id=chat_id)
        print(chat.updated)
        chat.updated = timezone.now()
        chat.save()
        print(chat.updated)
        return redirect("/ms/?im=" + str(chat_id))
