from django.shortcuts import render
from django.views.generic.base import View
from ...models import House, HousesManage


class HomeRetriveView(View):
    def get(self, request, address_pk):

        house = HousesManage.objects.get(id_address_in_base=address_pk)
        return render(request, "homeList/home_retrive.html", {"house": house})