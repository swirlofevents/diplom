from django.shortcuts import render, redirect
from django.views.generic.base import View
from ...models import House, User, Address
from manager_utils import bulk_upsert
from dateparse import main_cs
from datetime import datetime
import threading


class HomeListView(View):
    """Список домов"""

    def get(self, request):

        home_list = House.objects.all().order_by("outside", "house")
        return render(
            request,
            "homeList/home_list.html",
            {
                "home_list": home_list,
            },
        )

    def update_homelist(self, request):
        print("Я получил запрос на обновление")
        # result = test.main()
        if User.objects.get(id=request.user.id).is_superuser:

            """Обновление домов в управлении"""
            try:
                er = f"Пробую найти запрос на обновление списка домов\nНе нашел"
                main_cs.get_test()
            except:
                print(er)

            try:
                er = f"Пробую найти запрос на обновление информации домов\nНе нашел"
                if request.POST["info_in_address_update"]:
                    print("сделал обращение к функции get_address_info")
                    data = main_cs.get_address_info()
                    print("получил ответ от функции get_address_info")
                    for info in data:
                        obj = House.objects.get(address_id=info["address_id"])
                        print("получил дом")
                        obj.area_life = info["area_life"]
                        print("получил area_life")
                        obj.floors = int(info["floors"])
                        print("получил floors")
                        obj.entrances = int(info["entrances"])
                        print("получил entrances")
                        obj.year_of_house = int(info["year_of_house"])
                        print("получил year_of_house")
                        obj.managment_file = info["managment_file"]
                        print("получил managment_file")
                        obj.save()
                        print("сохранил")
            except:
                print(er)

    def post(self, request):
        update_thread = threading.Thread(
            target=self.update_homelist,
            args=[
                request,
            ],
        )
        update_thread.start()
        print("я запустил и пошел")
        return redirect(
            "homelist",
        )
