from django.shortcuts import render, redirect
from django.views.generic.base import View
from ...models import UserRequests, User


class DispetherRequestsView(View):
    """Список обращений жильца"""

    def get(
        self,
        request,
    ):
        requests = UserRequests.objects.all()
        employee = User.objects.filter(is_staff=True)
        requests_in_work, requests_canceled, requests_work_out, requests_consideration = [], [], [], []
        for item in requests:
            if item.is_archive == False:
                if item.state == UserRequests.PERFORMED:
                    requests_work_out.append(item)
                elif item.state == UserRequests.CANCELLATION:
                    requests_canceled.append(item)
                elif item.state == UserRequests.SENT:
                    requests_consideration.append(item)
                else:
                    requests_in_work.append(item)
        return render(
            request,
            "requests/users_requests.html",
            {
                "all": requests,
                "employee": employee,
                "requests_in_work": requests_in_work,
                "requests_canceled": requests_canceled,
                "requests_work_out": requests_work_out,
                "requests_consideration": requests_consideration,
            },
        )

    def post(self, request, request_id):
        employee = User.objects.get(pk=request.POST.get("employee"))
        request_user = UserRequests.objects.get(id=request_id)
        request_user.employee = employee
        request_user.state = "Принята в работу"
        request_user.save()

        return redirect("/requests_disp/")