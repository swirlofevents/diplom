from .request_add import RequestAddView
from .request_delete import RequestDeleteView
from .request_star import RequestStar
from .request_view import UserRequestsView
from .users_requests_view import DispetherRequestsView