from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.base import View
from ...models import UserRequests


class RequestDeleteView(View):
    """Удаление обращения жильца"""

    def get(self, request, request_pk):
        request = get_object_or_404(UserRequests, pk=request_pk)
        if request.is_canceled or request.state == UserRequests.PERFORMED:
            request.is_archive = True
            request.state = "Заявка в архиве"
            request.save()
        else:
            request.is_canceled = True
            request.state = "Заявка отменена"
            request.save()

        return redirect("/requests/")