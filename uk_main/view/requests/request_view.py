from django.shortcuts import render, redirect
from django.views.generic.base import View
from ...models import UserRequests, House
from django.core.exceptions import ObjectDoesNotExist
from ..utilits import get_values


class UserRequestsView(View):
    """Список обращений жильца"""

    def get(
        self,
        request,
    ):
        types = get_values(UserRequests.REQUEST_TYPE)
        requests = UserRequests.objects.filter(user=request.user.id)
        requests_in_work, requests_canceled, requests_work_out = [], [], []
        stars = get_values(UserRequests.REQUEST_STARS)
        """Проверка на привязку дома к пользователю"""
        try:
            houses_lodger = House.objects.filter(lodger=request.user.id)
            if len(houses_lodger) == 0:
                house_exist = False
            else:
                house_exist = True
        except ObjectDoesNotExist:
            house_exist = False
        for item in requests:
            if item.is_archive == False:
                if item.state == UserRequests.PERFORMED:
                    requests_work_out.append(item)
                elif item.state == UserRequests.CANCELLATION:
                    requests_canceled.append(item)
                else:
                    requests_in_work.append(item)
        return render(
            request,
            "requests/requests_list.html",
            {
                "requests_in_work": requests_in_work,
                "requests_canceled": requests_canceled,
                "requests_work_out": requests_work_out,
                "requests_type": types,
                "request_stars": stars,
                "house_exist": house_exist,
                "houses_lodger": houses_lodger,
            },
        )