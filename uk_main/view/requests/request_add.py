from ...forms import UserRequestsForm
from datetime import datetime
from django.shortcuts import redirect
from django.views.generic.base import View


class RequestAddView(View):
    """Создание нового обращения жильца"""

    def post(self, request):
        form = UserRequestsForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.user_id = request.user.id
            form.date = datetime.now()
            form.save()

        return redirect("/requests/")