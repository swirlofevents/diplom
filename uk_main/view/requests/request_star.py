from django.shortcuts import redirect
from django.views.generic.base import View
from ...models import UserRequests


class RequestStar(View):
    """Сохранение оценки заявки"""

    def get(self, request, pk):
        request_obj = UserRequests.objects.get(id=pk)
        if request_obj.user.id == request.user.id:
            request_obj.request_stars = request.GET["request_stars"]
            request_obj.save()
        return redirect("/requests/")