from .meter_add import WaterMeterAddView
from .meter_delete import WaterMeterDeleteView
from .meter_view import WaterMeterView