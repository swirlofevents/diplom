from django.shortcuts import redirect
from django.views.generic.base import View
from django.shortcuts import get_object_or_404
from ...models import WaterMeter


class WaterMeterDeleteView(View):
    """Удаление показаний жильца"""

    def get(self, request, meter_pk):
        meter = get_object_or_404(WaterMeter, pk=meter_pk)
        if meter.is_archive:
            meter.delete()
        else:
            meter.is_archive = True
            meter.save()

        return redirect("/meters/")