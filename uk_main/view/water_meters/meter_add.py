from uk_main.models.houses import House
from django.shortcuts import redirect
from django.views.generic.base import View
from ...models import House
from ...forms import UserWaterMeterForm


class WaterMeterAddView(View):
    """Создание новых показаний жильца"""

    def post(self, request):
        form = UserWaterMeterForm(request.POST)
        apartament = House.objects.filter(lodger=request.user.id).first()
        if form.is_valid():
            print(request.POST)
            form = form.save(commit=False)
            form.lodger_id = request.user.id
            form.apartament = apartament
            form.save()

        return redirect("/meters/")