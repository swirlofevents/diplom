from django.shortcuts import render
from django.views.generic.base import View
from ..utilits import get_values
from ...models import WaterMeter, House
from django.core.exceptions import ObjectDoesNotExist


class WaterMeterView(View):
    """Список показаний жильца"""

    def get(self, request):
        locations = get_values(WaterMeter.LOCATION_DEVICE)
        types = get_values(WaterMeter.TYPE_DEVICE)
        waterMeters = WaterMeter.objects.filter(lodger=request.user.id)

        """Проверка на привязку дома к пользователю"""
        try:
            houses_lodger = House.objects.filter(lodger=request.user.id)
            if len(houses_lodger) == 0:
                house_exist = False
            else:
                house_exist = True
        except ObjectDoesNotExist:
            house_exist = False

        return render(
            request,
            "waterMeters/water_meters_list.html",
            {
                "meters_list": waterMeters,
                "meters_type": types,
                "meters_locate": locations,
                "house_exist": house_exist,
                "houses_lodger": houses_lodger,
            },
        )