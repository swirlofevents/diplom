from django.views.generic.base import View
from django.shortcuts import render
from ...models import PerfomedWork, Address


class PerfomedWorksListView(View):
    """
    Страница выполненых работ по текущему ремонту
    """

    def get(self, request):
        address_exstists = []
        address_list = Address.objects.all()
        for address in address_list:
            if PerfomedWork.objects.filter(address=address.id).first():
                address_exstists.append(address)
        perfomed_works_list = PerfomedWork.objects.all()
        return render(
            request,
            "perfomed_work/perfomed_work.html",
            {
                "perfomed_works_list": perfomed_works_list,
                "address_list": address_exstists,
            },
        )


class PerfomedWorkRetrive(View):
    """
    Стр
    """

    def get(self, request, address_pk):
        works_list = PerfomedWork.objects.filter(address=address_pk)
        years = []
        for work in works_list:
            years.append(work.year)
        years = set(years)
        values = []
        for year in years:
            works_year = works_list.filter(year=year)
            quarters = []
            for work in works_year:
                quarters.append(work.quarter)
            quarters = set(quarters)
            for quarter in quarters:
                values.append([year, quarter])

        return render(
            request,
            "perfomed_work/perfomed_work_ret.html",
            {"values_list": values, "address": Address.objects.get(pk=address_pk)},
        )


class PerfomedWorkRetriveByQuarterYear(View):
    def get(self, request, address_pk, quarter, year):
        file_list = PerfomedWork.objects.filter(
            address=address_pk, quarter=str(quarter), year=str(year)
        ).order_by("year")
        return render(
            request,
            "perfomed_work/perfomed_work_ret_quarter_year.html",
            {
                "address": Address.objects.get(pk=address_pk),
                "file_list": file_list,
                "quarter": quarter,
                "year": year,
            },
        )
