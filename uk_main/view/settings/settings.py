from uk_main.models import profile_image
from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.core.exceptions import ObjectDoesNotExist
from ...models import House, User, ProfileImage


class SettingsView(View):
    """Страница настроек"""

    def get(self, request, user_id):
        house_list = House.objects.filter(lodger=None)
        try:
            lodger_house = House.objects.get(lodger=request.user.id)
        except ObjectDoesNotExist:
            lodger_house = None
        return render(
            request,
            "account/settings.html",
            {
                "houses": house_list,
                "lodger_house": lodger_house,
                "user_profile": User.objects.get(id=user_id),
            },
        )

    def post(self, request):
        try:
            house = House.objects.get(pk=request.POST.get("house"))
            if house.lodger == None:
                house.lodger = User.objects.get(pk=request.user.id)
                house.save()
            else:
                return redirect("/profile/" + str(request.user.id))
        except ObjectDoesNotExist:
            return redirect("/profile/" + str(request.user.id))

        return redirect("/profile/" + str(request.user.id))