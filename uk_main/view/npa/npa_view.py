from django.views.generic.base import View
from django.shortcuts import render
from ...models import NPA


class NPAView(View):
    """
    Страница нормативно-правовых актов
    """

    def get(self, request):
        npa_list = NPA.objects.all()
        return render(
            request,
            "npa/npa.html",
            {
                "npa_list": npa_list,
            },
        )
