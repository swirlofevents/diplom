from django.db.models import Model, CharField, TextField, FileField


class NPA(Model):
    file_name = CharField("Имя файла, которое будет отображаться на сайте", max_length=100)
    description = TextField(
        "Описание, которое отображается перед файлом", default=None, blank=True, null=True
    )
    file = FileField("Файл", upload_to="npa")

    def __str__(self) -> str:
        return self.file_name

    class Meta:
        verbose_name = "Налогово-правовой акт"
        verbose_name_plural = "Налогово-правовые акты"