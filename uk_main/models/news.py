from django.db.models import Model, CharField, TextField, DateField, ImageField


class News(Model):
    title = CharField("Наименование новости", max_length=255)
    content = TextField("Текст новости")
    publicate_date = DateField("Дата публикации")
    image = ImageField(upload_to="imageofnews", default="default.jpg")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"