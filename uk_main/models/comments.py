from .news import News
from django.db.models import Model, ForeignKey, TextField, CASCADE, SET_NULL, DateTimeField
from .user import CustomUser as User
from datetime import datetime


class NewsComments(Model):
    news = ForeignKey(
        News,
        on_delete=CASCADE,
        related_name="comments",
        blank=True,
        null=True,
        default=None,
        verbose_name="Новость",
    )
    user = ForeignKey(
        User,
        on_delete=CASCADE,
        related_name="comments",
        blank=True,
        null=True,
        default=None,
        verbose_name="Пользователь",
    )
    text = TextField("Текст комментария", default=None, blank=True, null=True)
    parent = ForeignKey(
        "self",
        verbose_name="Родитель",
        related_name="child_comments",
        on_delete=SET_NULL,
        blank=True,
        null=True,
    )
    date = DateTimeField("Дата написания", default=datetime.now, blank=True, null=True)

    def __str__(self):
        return f"{self.news} - {self.user}"

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"