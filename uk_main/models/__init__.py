from .comments import NewsComments
from .houses import Address, House
from .news import News
from .notifications import Notification, NotificationRead
from .npa import NPA
from .perfomed_work import PerfomedWork
from .user_request import UserRequests
from .water_meter import WaterMeter
from .chats import Chat, Message
from .user import CustomUser as User
from .house_list import HousesManage
from .profile_image import ProfileImage