from django.db.models import Model, ForeignKey, ImageField, CASCADE
from .user import CustomUser as User


class ProfileImage(Model):

    user = ForeignKey(User, verbose_name="Пользователь", related_name="profile_image", on_delete=CASCADE)
    profile_image = ImageField(upload_to="profile_images", default="default_profile.png")

    def __str__(self):
        return self.user.get_short_name()

    class Meta:
        verbose_name = "Фотография пользователя"
        verbose_name_plural = "Фотографии пользователей"
