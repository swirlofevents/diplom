from django.db.models import Model, DateTimeField, TextField, CharField, ForeignKey, BooleanField, CASCADE
from datetime import datetime
from .user import CustomUser as User


class Notification(Model):
    date_start = DateTimeField("Дата начала оповещения", default=datetime.now, blank=True)
    date_end = DateTimeField("Дата оканчания оповещения", default=datetime.now, blank=True)
    content = TextField("Текст оповещения")
    house = CharField("Улицы", max_length=255, default=" ")

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = "Оповещение"
        verbose_name_plural = "Оповещения"


class NotificationRead(Model):
    notificate = ForeignKey(Notification, on_delete=CASCADE)
    user = ForeignKey(User, on_delete=CASCADE)
    isRead = BooleanField("Статус")