from .user_manager import CustomUserManager
from django.db.models import EmailField, BooleanField
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


class CustomUser(AbstractUser):
    username = None
    email = EmailField(_("email address"), unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def get_short_name(self) -> str:
        return f"{self.last_name} {self.first_name[0]}."

    def __str__(self) -> str:
        return f"{self.last_name} {self.first_name}"
