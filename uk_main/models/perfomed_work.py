from django.db.models import Model, CharField, TextField, FileField, ForeignKey, CASCADE
from .houses import Address


class PerfomedWork(Model):
    file_name = CharField("Имя файла, которое будет отображаться на сайте", max_length=100)
    description = TextField(
        "Описание, которое отображается перед файлом", default=None, blank=True, null=True
    )
    file = FileField("Файл", upload_to="perfomed_work")
    quarter = CharField("Квартал", max_length=255, blank=True, null=True, default=None)
    year = CharField("Год", max_length=255, blank=True, null=True, default=None)
    address = ForeignKey(
        Address, on_delete=CASCADE, blank=True, null=True, default=None, verbose_name="Адрес"
    )

    def __str__(self) -> str:
        return self.file_name

    class Meta:
        verbose_name = "Выполненные работы по текущему ремонту"
        verbose_name_plural = "Выполненные работы по текущему ремонту"