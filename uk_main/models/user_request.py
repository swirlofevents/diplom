from django.db.models import Model, CharField, TextField, BooleanField, ForeignKey, DateTimeField, DO_NOTHING
from .user import CustomUser as User
from django.utils.translation import gettext_lazy as _


class UserRequests(Model):
    """Типы заявок"""

    TUBE = "Протечка трубы"
    DEVICE = "Опломбировать/поставить/заменить прибор учета воды"
    TYALET = "Сломался туалет"
    REQUEST_TYPE = (
        (TUBE, "Протечка трубы"),
        (DEVICE, "Опломбировать/поставить/заменить прибор учета воды"),
        (TYALET, "Сломался туалет"),
    )

    """Статусы заявок"""
    SENT = "На рассмотрении"
    ACCEPTED = "Принята в работу"
    POSTPONED = "Заявка перенесена"
    PERFORMED = "Заявка выполнена"
    CANCELLATION = "Заявка отменена"
    ARCHIVE = "Заявка в архиве"
    REQUEST_STATE = (
        (SENT, "На рассмотрении"),
        (ACCEPTED, "Принята в работу"),
        (POSTPONED, "Заявка перенесена"),
        (PERFORMED, "Заявка выполнена"),
        (CANCELLATION, "Заявка отменена"),
        (ARCHIVE, "Заявка в архиве"),
    )

    """Оценка заявки"""
    EXCELLENT = _("Отлично")
    GOOD = _("Хорошо")
    OKEY = _("Удовлетворительно")
    BAD = _("Плохо")
    AWFUL = _("Ужасно")
    NOT = _("Не задано")
    REQUEST_STARS = (
        (EXCELLENT, _("Отлично")),
        (GOOD, _("Хорошо")),
        (OKEY, _("Удовлетворительно")),
        (BAD, _("Плохо")),
        (AWFUL, _("Ужасно")),
        (NOT, _("Не задано")),
    )

    user = ForeignKey(User, related_name="user_requests", on_delete=DO_NOTHING)
    employee = ForeignKey(
        User,
        related_name="employee_exec_requests",
        on_delete=DO_NOTHING,
        blank=True,
        null=True,
        default=None,
    )
    date = DateTimeField("Дата подачи заявки")
    title = CharField("Тип заявки", max_length=100, choices=REQUEST_TYPE, default=TUBE)
    descriptions = TextField("Описание заявки")
    state = CharField("Статус заявки", max_length=20, choices=REQUEST_STATE, default=SENT)
    is_canceled = BooleanField("Отменена заявка или нет", default=False)
    request_stars = CharField(
        "Оценка",
        max_length=17,
        choices=REQUEST_STARS,
        default=NOT,
    )
    is_archive = BooleanField("Заявка перенесена в архив и не отображается у пользователя", default=False)

    def __str__(self):
        return "Пользователь {} {}. Статус заявки: {}.".format(self.user, self.title, self.state)

    class Meta:
        verbose_name = "Заявка жильца"
        verbose_name_plural = "Заявки жильцов"