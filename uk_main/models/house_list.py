from django.db.models import (
    Model,
    CharField,
    TextField,
    DateField,
)


class HousesManage(Model):
    id_address_in_base = CharField("Id дома в базе РеформаЖКХ", max_length=100, blank=True, null=True)
    address = CharField("Адрес", max_length=100, blank=True, null=True)
    year_build = CharField("Год постройки", max_length=100, blank=True, null=True)
    commissioning_year = CharField("Год ввода в эксплуатацию", max_length=100, blank=True, null=True)
    serias_type_of_construction = CharField(
        "Серия, тип постройки здания", max_length=100, blank=True, null=True
    )
    residential_building_type = CharField("Тип дома", max_length=100, blank=True, null=True)
    method_of_forming_a_capital_repair_fund = CharField(
        "Способ формирования фонда капитального ремонта", max_length=100, blank=True, null=True
    )
    floor_max = CharField("Наибольшее количество этажей", max_length=100, blank=True, null=True)
    floor_min = CharField("Наименьшее количество этажей", max_length=100, blank=True, null=True)
    entrances = CharField("Количество подъездов", max_length=100, blank=True, null=True)
    elevators = CharField("Количество лифтов", max_length=100, blank=True, null=True)
    energy_efficiency_class = CharField(
        "Класс энергетической эффективности", max_length=100, blank=True, null=True
    )
    rooms_count = CharField("Количество помещений, всего", max_length=100, blank=True, null=True)
    rooms_count_life = CharField("Количество жилых помещений", max_length=100, blank=True, null=True)
    rooms_count_no_life = CharField("Количество нежилых помещений", max_length=100, blank=True, null=True)
    total_area_of_premises = CharField("Общая площадь дома, всего", max_length=100, blank=True, null=True)
    total_area_of_life = CharField("Общая площадь жилых помещений", max_length=100, blank=True, null=True)
    total_area_of_no_life = CharField(
        "Общая площадь нежилых помещений", max_length=100, blank=True, null=True
    )
    total_area_of_common_property = CharField(
        "Общая площадь помещений, входящих в состав общего имущества", max_length=100, blank=True, null=True
    )
    land_area = CharField(
        "Площадь земельного участка, входящего в состав общего имущества в многоквартирном доме",
        max_length=100,
        blank=True,
        null=True,
    )
    parking_area = CharField(
        "Площадь парковки в границах земельного участка", max_length=100, blank=True, null=True
    )
    playground = CharField(
        "Элементы благоустройства (детская площадка)", max_length=100, blank=True, null=True
    )
    sportground = CharField(
        "Элементы благоустройства (спортивная площадка)", max_length=100, blank=True, null=True
    )
    other_improvement_elements = CharField(
        "Элементы благоустройства (другое)", max_length=100, blank=True, null=True, default="Не задано"
    )
    additional_information = TextField("Доп. информация", blank=True, null=True, default="Не задано")
    basis_for_managment = CharField("Основания для управления", max_length=256, blank=True, null=True)
    date_managment_start = DateField("Дата начала управления", blank=True, null=True)
    reforma_zkh_url = TextField("Ссылка на реформу жкх", blank=True, null=True)
    cadastr_number = CharField("Кадастрвоый номер", max_length=100, blank=True, null=True)

    def __str__(self):
        return self.address

    def get_file_name(self):

        return self.managment_file.name.split("/")[1]

    class Meta:
        verbose_name = "Дом у правлении"
        verbose_name_plural = "Дома в управлении"
