from django.db.models import Model, CharField, IntegerField, BooleanField, ForeignKey, CASCADE, DO_NOTHING
from .user import CustomUser as User
from .houses import House


class WaterMeter(Model):
    """Тип прибора"""

    COLD = "ХВ"
    HOT = "ГВ"
    NOTINDICATED = "НН"
    TYPE_DEVICE = (
        (COLD, "ХВ"),
        (HOT, "ГВ"),
        (NOTINDICATED, "НН"),
    )

    """Местоположение прибора"""
    KITCHEN = "Кухня"
    BATHROOM = "Ванная"
    RESTROOM = "С/У"
    CORRIDOR = "Коридор"
    PANTRY = "Кладовая"
    LOCATION_DEVICE = (
        (KITCHEN, "Кухня"),
        (BATHROOM, "Ванная"),
        (RESTROOM, "С/У"),
        (CORRIDOR, "Коридор"),
        (PANTRY, "Кладовая"),
    )

    type = CharField("Тип прибора (ХВ/ГВ)", max_length=2, choices=TYPE_DEVICE, default=NOTINDICATED)
    sires_number = CharField("Серийний номер", max_length=40, blank=True, null=True, default=None)
    location = CharField(
        "Местоположение в квартире",
        max_length=20,
        choices=LOCATION_DEVICE,
        default=BATHROOM,
    )
    apartament = ForeignKey(House, on_delete=CASCADE)
    current_testimony = IntegerField("Текущие показания", default=0)
    last_testimony = IntegerField("Последние показания", blank=True, null=True, default=0)
    lodger = ForeignKey(User, on_delete=DO_NOTHING, blank=True, null=True, default=None)
    is_archive = BooleanField("Удален или нет ПУ", default=False)

    def __str__(self):
        return "Тип прибора: {}. Серийный номер: {}. Адрес: {} д.{} кв.{}".format(
            self.type,
            self.sires_number,
            self.apartament.outside,
            str(self.apartament.house),
            str(self.apartament.apartament),
        )

    class Meta:
        verbose_name = "Показание прибора учета воды"
        verbose_name_plural = "Показания проборов учета воды"