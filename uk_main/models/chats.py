from django.db.models import (
    Model,
    CharField,
    TextField,
    BooleanField,
    ForeignKey,
    DateTimeField,
    ManyToManyField,
    NOT_PROVIDED,
    DO_NOTHING,
    SET_NULL,
)
from django.utils import timezone
from .user import CustomUser as User


class Chat(Model):

    DIALOG = "D"
    CHAT = "C"
    CHAT_TYPE_CHOICES = ((DIALOG, "Dialog"), (CHAT, "Chat"))

    type = CharField("Тип", max_length=1, choices=CHAT_TYPE_CHOICES, default=DIALOG)
    members = ManyToManyField(User, verbose_name="Участник", related_name="chats")
    updated = DateTimeField("Последнее обновление чата", default=timezone.now)

    def __str__(self):
        members = ""
        for member in self.members.all():
            members += member.get_short_name() + ", "
        return "Тип чата {}. Участники: {}.".format(self.type, members)

    class Meta:
        verbose_name = "Чат"
        verbose_name_plural = "Чаты"


class Message(Model):
    chat = ForeignKey(Chat, verbose_name="Чат", null=True, blank=True, on_delete=SET_NULL)
    author = ForeignKey(User, verbose_name="Пользователь", null=True, blank=True, on_delete=SET_NULL)
    message = TextField("Сообщение")
    pub_date = DateTimeField("Дата сообщения", default=timezone.now)
    is_readed = BooleanField("Прочитано", default=False)

    def __str__(self):
        return "Отправитель {} Чат {}".format(self.author, self.chat)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        ordering = ["pub_date"]