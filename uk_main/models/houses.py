from django.db.models import (
    Model,
    CharField,
    TextField,
    IntegerField,
    FileField,
    DateField,
    ForeignKey,
    CASCADE,
)
from .user import CustomUser as User


class Address(Model):
    street_name = CharField("Улица", max_length=255)

    def __str__(self) -> str:
        return self.street_name

    class Meta:
        verbose_name = "Адрес"
        verbose_name_plural = "Адреса"


class House(Model):
    address_id = CharField("Id дома в базе РеформаЖКХ", max_length=100, blank=True, null=True)
    outside = CharField("Улица", max_length=100)
    house = CharField("Дом", max_length=100)
    body = CharField("Корпус", max_length=100, blank=True, null=True)
    apartament = IntegerField("Квартира", blank=True, null=True)
    area = CharField("Общая площадь", max_length=100, blank=True, null=True)
    basis_for_managment = CharField("Основания для управления", max_length=256, blank=True, null=True)
    date_managment_start = DateField("Дата начала управления", blank=True, null=True)
    area_life = CharField("Жилая площадь", max_length=100, blank=True, null=True)
    floors = IntegerField("Количество этажей", blank=True, null=True)
    entrances = IntegerField("Количество подъездов", blank=True, null=True)
    year_of_house = IntegerField("Год ввода дома в эксплуатацию", blank=True, null=True)
    managment_file = FileField(
        "Файл, приказ, основание для управления", upload_to="manage_file_house", blank=True, null=True
    )
    reforma_zkh_url = TextField("Ссылка на реформу жкх", blank=True, null=True)
    lodger = ForeignKey(
        User, on_delete=CASCADE, blank=True, null=True, default=None, verbose_name="Владелец"
    )

    def __str__(self):

        """Проверка корпуса"""
        if self.body:
            body = f"{self.body} "
        else:
            body = " "

        """Проверка Квартиры"""
        if self.apartament:
            apartament = f"кв. {self.apartament}"
        else:
            apartament = " "

        return "{} д.{}{}".format(self.outside, str(self.house) + body, apartament)

    def get_file_name(self):

        return self.managment_file.name.split("/")[1]

    class Meta:
        verbose_name = "Дом у правлении"
        verbose_name_plural = "Дома в управлении"