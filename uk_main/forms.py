from __future__ import absolute_import
from uk_main.models.comments import NewsComments

import warnings
from importlib import import_module

from django import forms
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions, validators
from django.urls import reverse
from django.utils.translation import gettext, gettext_lazy as _, pgettext
from allauth.account.forms import SignupForm, LoginForm

from .models import UserRequests, WaterMeter
from .models import User


class CustomUserSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, label="Ваше имя", required=True)
    last_name = forms.CharField(max_length=30, label="Ваша фамилия", required=True)

    def save(self, request):
        user = super(CustomUserSignupForm, self).save(request)
        user.first_name = self.cleaned_data["first_name"].capitalize()
        user.last_name = self.cleaned_data["last_name"].capitalize()
        user.save()
        return user


class UserRequestsForm(forms.ModelForm):
    """Форма для обращения жильцов"""

    class Meta:
        model = UserRequests
        fields = ("title", "descriptions")


class UserWaterMeterForm(forms.ModelForm):
    """Форма для показаний счетчик воды"""

    class Meta:
        model = WaterMeter
        fields = ("type", "location", "sires_number", "current_testimony")


class CreateCommentForm(forms.ModelForm):
    """Форма для создания комментариев"""

    class Meta:
        model = NewsComments
        fields = ("text",)