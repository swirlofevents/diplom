from uk_main.view.chats.chats.create_chat import ChatCreateView
from django.urls import path

from . import views

urlpatterns = [
    path("", views.MainView.as_view(), name="main"),
    path("news", views.NewsListView.as_view(), name="news"),
    path("news/<int:news_id>", views.NewsRetriveView.as_view(), name="news_retrive"),
    path("comment/<int:news_id>", views.CreateCommentView.as_view(), name="comment_create"),
    path("comment/delete/<int:comment_id>", views.CommentDeleteView.as_view(), name="comment_delete"),
    path("npa", views.NPAView.as_view(), name="npa"),
    path("pw", views.PerfomedWorksListView.as_view(), name="perfomed_work"),
    path("pw/<int:address_pk>/", views.PerfomedWorkRetrive.as_view(), name="pw"),
    path(
        "pw/<int:address_pk>/<int:year>/<int:quarter>",
        views.PerfomedWorkRetriveByQuarterYear.as_view(),
        name="pwQY",
    ),
    path("license", views.LicenseView.as_view(), name="license"),
    path("requisite", views.RequisitesView.as_view(), name="requisites"),
    path("homelist", views.HomeListView.as_view(), name="homelist"),
    path("homelist/<int:address_pk>", views.HomeRetriveView.as_view(), name="home_info"),
    path("profile/<int:user_id>", views.SettingsView.as_view(), name="settings"),
    path("set_address/", views.SettingsView.as_view(), name="set_address"),
    path("requests_disp/", views.DispetherRequestsView.as_view(), name="reqeusts_dispetcher"),
    path(
        "requests_disp/<int:request_id>/", views.DispetherRequestsView.as_view(), name="reqeusts_dispetcher"
    ),
    path("requests/", views.UserRequestsView.as_view(), name="request"),
    path("requests/create/", views.RequestAddView.as_view(), name="request_add"),
    path("request/star/<int:pk>", views.RequestStar.as_view(), name="request_star"),
    path(
        "requests/delete/<int:request_pk>/",
        views.RequestDeleteView.as_view(),
        name="request_delete",
    ),
    path("chat/create/<int:user_id>", views.ChatCreateView.as_view(), name="create_chat"),
    path("ms/", views.ChatListView.as_view(), name="chats"),
    path("ms/create/<int:chat_id>", views.CreateMessageView.as_view(), name="create_mess"),
    path("meters/", views.WaterMeterView.as_view(), name="water_meters"),
    path("meters/create/", views.WaterMeterAddView.as_view(), name="water_meters_add"),
    path(
        "meters/delete/<int:meter_pk>/",
        views.WaterMeterDeleteView.as_view(),
        name="water_meters_delete",
    ),
]