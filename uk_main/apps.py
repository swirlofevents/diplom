from django.apps import AppConfig


class UkMainConfig(AppConfig):
    name = 'uk_main'
