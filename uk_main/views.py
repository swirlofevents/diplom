from .view import (
    MainView,
    PerfomedWorksListView,
    PerfomedWorkRetriveByQuarterYear,
    PerfomedWorkRetrive,
    NPAView,
    CommentDeleteView,
    CreateCommentView,
    CreateMessageView,
    ChatListView,
    ChatCreateView,
    RequestAddView,
    RequestDeleteView,
    RequestStar,
    UserRequestsView,
    WaterMeterDeleteView,
    WaterMeterView,
    WaterMeterAddView,
    NewsRetriveView,
    NewsListView,
    SettingsView,
    LicenseView,
    RequisitesView,
    HomeListView,
    HomeRetriveView,
    DispetherRequestsView,
)
