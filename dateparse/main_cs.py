from .get_data.address_list_request import get_address_list_request
from .get_data.excel_parse import get_house_manage_info


def get_address_list():
    return get_address_list_request(
        "https://www.reformagkh.ru/mymanager/profile/8940443/?tid=2400411&isAll=&page=1&limit=100#content"
    )


def get_test():
    get_house_manage_info()
    get_address_list_request(
        "https://www.reformagkh.ru/mymanager/profile/8940443/?tid=2400411&isAll=&page=1&limit=100#content"
    )


if __name__ == "__main__":
    get_address_list()
