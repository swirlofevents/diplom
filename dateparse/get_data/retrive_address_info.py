import requests
import os
from bs4 import BeautifulSoup
import time
from .variables import headers, domain


def retrive_address_info(id, url):

    if os.path.exists(f"dateparse/html_files/house_info_page/{id}.html"):
        with open(f"dateparse/html_files/house_info_page/{id}.html") as file:
            print("Файл уже есть")
            src = file.read()
    else:
        with open(f"dateparse/html_files/house_info_page/{id}.html", "w") as file:
            print("Файла нет")
            req = requests.get(url=url, headers=headers)
            time.sleep(5)
            file.write(req.text)
            src = req.text

    soup = BeautifulSoup(src, "lxml")
    file_a = soup.find_all("table", class_="w-100 simple-table halfed")[1].find_all("td")[-2].find("a")
    file_name = file_a.text.split("\n")[1].replace(" ", "")

    if os.path.exists(f"media/manage_file_house/{file_name}.pdf"):
        print("Файл уже есть")
    else:
        try:
            file_url = domain + file_a.get("href")
        except:
            print("Вылезла капча, усну на 5 минут")
            time.sleep(300)
            with open(f"scrap/html_files/house_info_page/{id}.html", "w") as file:
                print("Файл нет")
                req = requests.get(url=url, headers=headers)
                time.sleep(5)
                file.write(req.text)
                src = req.text
            soup = BeautifulSoup(src, "lxml")
            file_a = (
                soup.find_all("table", class_="w-100 simple-table halfed")[1].find_all("td")[-2].find("a")
            )
            file_url = domain + file_a.get("href")

        with open(f"media/manage_file_house/{file_name}.pdf", "wb") as file:
            file.write(requests.get(file_url, "D:/").content)

    area_life = soup.find_all("div", class_="mr-5 flex-shrink-0")[
        1
    ].next_element.next_element.next_element.text
    floors = soup.find_all("div", class_="mr-5 flex-shrink-0")[2].next_element.next_element.next_element.text
    year_of_house = soup.find_all("div", class_="mr-5 flex-shrink-0")[
        3
    ].next_element.next_element.next_element.text
    entrances = soup.find_all("div", class_="mr-5 flex-shrink-0")[
        5
    ].next_element.next_element.next_element.text

    data = {
        "address_id": id,
        "area_life": area_life,
        "floors": floors,
        "year_of_house": year_of_house,
        "entrances": entrances,
        "managment_file": f"manage_file_house/{file_name}.pdf",
    }
    return data