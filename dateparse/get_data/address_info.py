import os
import json
from .address_list_request import get_address_list_request
from .retrive_address_info import retrive_address_info

address_list_request_url = (
    "https://www.reformagkh.ru/mymanager/profile/8940443/?tid=2400411&isAll=&page=1&limit=100#content"
)


def get_address_info():

    # req = requests.get(url=url, headers=headers)

    # """Для деплоя на хостинг"""

    # src = req.text

    # """Для работы"""
    if os.path.exists("dateparse/json_files/home_urls.json"):
        with open("dateparse/json_files/home_urls.json") as file:
            src = json.load(file)
    else:
        get_address_list_request(address_list_request_url)
        with open("dateparse/json_files/home_urls.json") as file:
            src = json.load(file)

    if os.path.exists("dateparse/json_files/home_urls_updated.json"):
        with open("dateparse/json_files/home_urls_updated.json") as file:
            src_check = json.load(file)

        check_id = []
        for check in src_check:
            check_id.append(check)

        data = []
        for href in src:
            if href not in check_id:
                data.append(retrive_address_info(href, src[href]))
    else:
        data = []
        for href in src:
            data.append(retrive_address_info(href, src[href]))

    with open("dateparse/json_files/home_urls_updated.json", "w", encoding="utf-8") as file:
        json.dump(src, file, indent=4, ensure_ascii=False)

    return data