import requests
import json
from bs4 import BeautifulSoup
from .variables import headers, domain
from uk_main.models import Address, House, HousesManage
from datetime import datetime


def get_address_list_request(url):

    req = requests.get(url=url, headers=headers)

    """Для деплоя на хостинг"""
    src = req.text
    """Для работы"""
    # if os.path.exists("scrap/html_files/index.html"):
    #     with open("scrap/html_files/index.html") as file:
    #         src = file.read()
    # else:
    #     with open("scrap/html_files/index.html", "w") as file:
    #         file.write(req.text)
    #         src = req.text

    soup = BeautifulSoup(src, "lxml")
    houses_list = soup.find("table", class_="table text-center").find("tbody").find_all("tr")
    address = []
    address_urls = {}
    for tr in houses_list:
        address_url = tr.select_one("td", class_="text-left").find("a")
        adres = address_url.text.split(",")
        outside = adres[1][5:]
        address.append(outside)

        house_manage_obj = HousesManage.objects.filter(
            id_address_in_base=address_url.get("href").split("/")[4]
        ).first()
        house_manage_obj.basis_for_managment = tr.find(
            "td", class_="text-nowrap"
        ).next_element.next_element.next_element.text
        house_manage_obj.date_managment_start = datetime.strptime(
            tr.find("td", class_="text-right").text, "%d.%m.%Y"
        ).date()
        house_manage_obj.reforma_zkh_url = domain + address_url.get("href")
        house_manage_obj.save()
        try:
            old_object = House.objects.get(address_id=address_url.get("href").split("/")[4])
        except:
            house_obj = House(
                address_id=address_url.get("href").split("/")[4],
                outside=outside,
                house=adres[2][4:],
                area=tr.find("td", class_="text-nowrap").text,
                basis_for_managment=tr.find(
                    "td", class_="text-nowrap"
                ).next_element.next_element.next_element.text,
                date_managment_start=datetime.strptime(
                    tr.find("td", class_="text-right").text, "%d.%m.%Y"
                ).date(),
            )

            try:
                body = adres[3][1:]
                house_obj.body = body
            except Exception:
                house_obj.body = None

            house_obj.reforma_zkh_url = domain + address_url.get("href")
            house_obj.save()

            address_urls[house_obj.address_id] = domain + address_url.get("href")

    ####### СОХРАНЕНИЕ ССЫЛОК НА ДОМА
    with open("dateparse/json_files/home_urls.json", "w", encoding="utf-8") as file:
        json.dump(address_urls, file, indent=4, ensure_ascii=False)

    return set(address)
