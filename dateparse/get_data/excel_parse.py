import openpyxl
import json
from uk_main.models import HousesManage
from .address_list_request import get_address_list_request


### INIT


def get_house_manage_info():
    excel_file = openpyxl.load_workbook("dateparse/get_data/Выгрузка МКД. ООО _УК на Пражской_.xlsx")
    employees_sheet = excel_file["Паспорт МКД"]
    cadastr = excel_file["Кадастровые паспорта МКД"]
    cadastr_rows = cadastr.iter_rows()
    data = []
    header_cells_generator = employees_sheet.iter_rows()
    for header_cells_tuple in header_cells_generator:
        try:
            object_exists = HousesManage.objects.get(id_address_in_base=header_cells_tuple[0].value)
            if object_exists:
                object_exists.id_address_in_base = header_cells_tuple[0].value
                object_exists.address = (
                    f"{header_cells_tuple[17].value} {header_cells_tuple[18].value} д.{header_cells_tuple[21].value} к.{header_cells_tuple[22].value}"
                    if header_cells_tuple[22].value
                    else f"{header_cells_tuple[17].value} {header_cells_tuple[18].value} д.{header_cells_tuple[21].value}"
                )

                object_exists.year_build = header_cells_tuple[28].value
                object_exists.commissioning_year = header_cells_tuple[29].value
                object_exists.serias_type_of_construction = header_cells_tuple[30].value
                object_exists.residential_building_type = header_cells_tuple[31].value
                object_exists.method_of_forming_a_capital_repair_fund = header_cells_tuple[33].value
                object_exists.floor_max = header_cells_tuple[34].value
                object_exists.floor_min = header_cells_tuple[35].value
                object_exists.entrances = header_cells_tuple[36].value
                object_exists.elevators = header_cells_tuple[37].value
                object_exists.energy_efficiency_class = header_cells_tuple[38].value
                object_exists.rooms_count = header_cells_tuple[39].value
                object_exists.rooms_count_life = header_cells_tuple[40].value
                object_exists.rooms_count_no_life = header_cells_tuple[41].value
                object_exists.total_area_of_premises = header_cells_tuple[42].value
                object_exists.total_area_of_life = header_cells_tuple[43].value
                object_exists.total_area_of_no_life = header_cells_tuple[44].value
                object_exists.total_area_of_common_property = header_cells_tuple[45].value
                object_exists.land_area = header_cells_tuple[46].value
                object_exists.parking_area = header_cells_tuple[47].value
                object_exists.playground = header_cells_tuple[48].value
                object_exists.sportground = header_cells_tuple[49].value
                object_exists.other_improvement_elements = "Не задано"
                object_exists.additional_information = "Не задано"
                object_exists.save()

        except:
            obj = HousesManage(
                id_address_in_base=header_cells_tuple[0].value,
                address=f"{header_cells_tuple[17].value} {header_cells_tuple[18].value} д.{header_cells_tuple[21].value} к.{header_cells_tuple[22].value}"
                if header_cells_tuple[22].value
                else f"{header_cells_tuple[17].value} {header_cells_tuple[18].value} д.{header_cells_tuple[21].value}",
                year_build=header_cells_tuple[28].value,
                commissioning_year=header_cells_tuple[29].value,
                serias_type_of_construction=header_cells_tuple[30].value,
                residential_building_type=header_cells_tuple[31].value,
                method_of_forming_a_capital_repair_fund=header_cells_tuple[33].value,
                floor_max=header_cells_tuple[34].value,
                floor_min=header_cells_tuple[35].value,
                entrances=header_cells_tuple[36].value,
                elevators=header_cells_tuple[37].value,
                energy_efficiency_class=header_cells_tuple[38].value,
                rooms_count=header_cells_tuple[39].value,
                rooms_count_life=header_cells_tuple[40].value,
                rooms_count_no_life=header_cells_tuple[41].value,
                total_area_of_premises=header_cells_tuple[42].value,
                total_area_of_life=header_cells_tuple[43].value,
                total_area_of_no_life=header_cells_tuple[44].value,
                total_area_of_common_property=header_cells_tuple[45].value,
                land_area=header_cells_tuple[46].value,
                parking_area=header_cells_tuple[47].value,
                playground=header_cells_tuple[48].value,
                sportground=header_cells_tuple[49].value,
                other_improvement_elements="Не задано",
                additional_information="Не задано",
            )
            obj.save()
        # for i in range(0, len(header_cells_tuple)):

    for cadastar_row in cadastr_rows:
        for_obj = HousesManage.objects.get(id_address_in_base=cadastar_row[0].value)
        for_obj.cadastr_number = str(cadastar_row[3].value)
        for_obj.save()

    # with open("dateparse/get_data/home_list.json", "w", encoding="utf-8") as file:
    #     json.dump(data, file, indent=4, ensure_ascii=False)


if __name__ == "__main__":
    get_house_manage_info()