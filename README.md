# Запуск проекта

- Скачать и установить Python с [официального сайта](https://www.python.org/downloads/)
- Установить все необходимые модули с помощью зависимостей:
`pip install -r requirements.txt`
- Скачать и установить PostgreSQL с [официального сайта](https://www.postgresql.org/)
- Настроить подключение к базе данных в файле ***uk-project/.env***
- Создать миграции для базы данных:
`python manage.py makemigrations`
- Применить миграции для базы данных:
`python manage.py migrate`
- Запустить сервер:
`python manage.py runserver`


